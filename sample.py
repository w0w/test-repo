from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect, Http404
from django.template.response import TemplateResponse
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from apps.base.decorators import only_GET, authenticated_user, only_POST, \
    either_GET_or_POST
from django.core.serializers import serialize
from django.utils import simplejson
from django.contrib.contenttypes.models import ContentType
from django.db.models import Avg, Count, Q, F
from django.contrib.auth.models import User
from django.conf import settings

from apps.course.models import Course, CoursePart, CoursePartResource, CourseFaq, CoursePartFollower, CoursePartRate, CourseReview, CourseLecture, CoursePartDone, CourseEnquiry
from apps.topics.models import Topic
from apps.library.models import Library
from apps.note.models import Note
from apps.course.forms import CourseForm, CourseEditLectureTitleForm,  CourseFormDict, QuestionForm, AnswerForm
from apps.base.utils import JSONResponse, get_int_or_default_from_request, paginate_from_request

from apps.permalink.utils import get_by_slug_or_404
from apps.api.serializer import  CoursePartsSerializer

from django_countries.countries import COUNTRIES


from copy import deepcopy
import json

from apps.assess.utils import *
from apps.assess.models import *

def course(request, slug=None):
    if slug:
        course, redirect = get_by_slug_or_404(Course, slug)

    if redirect:
        return HttpResponsePermanentRedirect(course.url())

    # needs review - vigman
    coursereviews = CourseReview.objects.filter(course=course)
    course_user_reviews_len = 0
    if request.user and request.user.is_authenticated():
        course_user_reviews_len = len(CourseReview.objects.filter(course=course, user=request.user))

    courseparts = CoursePart.objects.filter(course_id=course.id)

    # Modify for taken assignments
    takenTests = list()
    for part in course.parts.all():
        for res in part.resources.all():
            if res.object_type == 'assessment':
                test = Test.objects.get(pk=res.object_id)
                if request.user.is_authenticated():
                    sessions = TestSession.objects.filter(user=request.user, 
                    test=test)
                else:
                    sessions = []
                for sess in sessions:
                    sess.result = show_result(sess.session_key, False)
                    takenTests.append(sess)

    update_view_count = Course.objects.filter(id=course.id).update(view_count=F('view_count') + 1)
    from apps.payment.utils import can_view
    course.is_subscribed = can_view(request.user, course)
    page = 'basic'

    us  = CoursePart.objects.filter(course=course.id)
    part_ids = [x.id for x in us]
    obj = CoursePartDone.objects.filter(user=request.user)
    completed, notcompleted = list(), list()
    for x in obj:
        if x.id in part_ids:
            completed.append(x)
        else:
            notcompleted.append(x)
    for a in notcompleted:
        print a.course_part
        print a.id

    return render(request, 'course/course_page.html', {
        'course': course, 
        'courseparts': courseparts, 
        'debug': settings.DEBUG, 
        'coursereviews': coursereviews, 
        'course_user_reviews_len': course_user_reviews_len, 
        'page': page,
        'takenTests': takenTests,
        'announcements': MyAnnouncement.objects.filter(course=course),
        'notcompleted' : [x.course_part.id for x in notcompleted],
    })

def course_curriculum(request, slug=None, page_type=None):
    course, redirect = get_by_slug_or_404(Course, slug)
    if redirect:
        return HttpResponsePermanentRedirect(course.url()+(page_type if page_type else ''))
    if request.is_ajax():
        if page_type == 'basic':
            template = 'course/offline_course/objects/basic.html'
        elif page_type == 'curriculum':
            template = 'course/offline_course/objects/curriculum.html'
        elif page_type == 'pricing':
            template = 'course/offline_course/objects/other.html'
        elif page_type == 'faqs':
            template = 'course/offline_course/objects/faqs.html'
    else:
        template = 'course/course_page.html'

    update_view_count = Course.objects.filter(id=course.id).update(view_count=F('view_count') + 1)
    from apps.payment.utils import can_view
    course.is_subscribed = can_view(request.user, course)

    return render(request, template, {'course': course, 'page': page_type,})

def course_part(request, course_slug=None, part_slug=None):
    if not course_slug or not part_slug:
        raise Http404

    course, course_redirect = get_by_slug_or_404(Course, course_slug)
    course_part, part_redirect = get_by_slug_or_404(CoursePart, part_slug)

    if not course_part.course == course:
        raise Http404

    if course_redirect or part_redirect:
        return HttpResponsePermanentRedirect(course_part.url())

    from apps.payment.utils import can_view
    course_part.is_subscribed = can_view(request.user, course_part.course) if course_part.is_paid else True
    if course.is_paid and not course_part.is_subscribed:
        return HttpResponse(status=403)

    if request.is_ajax():
         return TemplateResponse(request, 'course/section_page_article.html', {'course_part': course_part, 'course': course})
    
    rating_count = CoursePartRate.objects.filter(course_part=course_part).count()
    user_rating=0
    if request.user.is_authenticated():
        course_part_follower = CoursePartFollower.objects.get_or_create(course_part=course_part, user=request.user)[0] 
        user_rating = CoursePartRate.objects.get_or_create(course_part=course_part, user=request.user)[0].rating
    
    # needs review - vigman
    courseparts = CoursePart.objects.filter(course_id=course.id)
    return render(request, 'course/section_page.html', {'course': course, 'course_part': course_part, 'courseparts': courseparts, 'user_rating': user_rating, 'rating_count': rating_count, 'debug': settings.DEBUG})


def course_with_subscription(request):
    return render(request, 'course/course_page_with_subscription.html', {})


def course_consume(request):
    return render(request, 'course/course_consume.html', {})

def course_library_module(request):
    from apps.library.views import library_module
    if request.is_ajax():
        return library_module(request, 'course_create/course_create_objects/course_create_library_content.html')
    else:
        return library_module(request, 'course_create/course_create_objects/course_create_library_resources.html')


@authenticated_user
def course_create(request, uuid=None):
    if request.method == 'POST':
        #uuid = request.REQUEST.get('id', None)
        if uuid:
            new_course = get_object_or_404(Course, uuid=uuid)
        else:
            new_course = Course(user=request.user)
        form = CourseForm(request.POST, instance=new_course)
        if form.is_valid():
            t = form.save()

            return JSONResponse({'status': 'success', 'msg': 'Course Created', 'next_url': reverse('course_create_offline', args=('basic', t.uuid)) if t.is_offline else reverse('course_create_dashboard', args=('overview', t.uuid)) })
            #return HttpResponseRedirect(reverse('course_create_dashboard', args=('getstarted', t.uuid)))
        return JSONResponse({'status': 'error', 'msg': 'Title is not valid', 'next_url': ''})
    return render(request, 'course_create/course_create_step1.html', {})

@authenticated_user
@only_POST
def course_publish(request, uuid=None):
    course = get_object_or_404(Course, uuid=uuid)
    action = request.REQUEST.get('action', '')

    if action not in ['publish', 'unpublish']:
        return JSONResponse({'status': 'error', 'msg': 'invalid action'})

    if request.user != course.user:
        return JSONResponse({'status': 'error', 'msg': 'You Dont have Permission to Publish this Course.'})

    if action == 'publish':
        course.is_published = True
        course.is_deleted = False
    elif action == 'unpublish':
        course.is_published = False
        course.is_deleted = False
    course.save()
    if action == 'publish':
        from apps.notifications.objects import UserObject, CourseObject
        from apps.notifications.events import CoursePublishEvent
        CoursePublishEvent(publisher=UserObject(course.user), subject=CourseObject(course)).process()

    return JSONResponse({'status': 'success', 'msg': 'Course %sed' %action})

@authenticated_user
def course_create_dashboard(request, page=None, uuid=None):
    if uuid:
        course = get_object_or_404(Course, uuid=uuid)
    else:
        course = None
    if request.method=="POST" and course:
        if page=='video' and request.FILES:
                request.FILES['video'] = request.FILES.getlist('file[]')[0]
                print 'files are available'
        form = CourseFormDict[page](request.POST, request.FILES, instance=course)
        if form.is_valid():
            t = form.save()
            return JSONResponse({'status': 'success', 'msg': '%s updated' %page, 'thumbnail_url': t.thumb_or_default__230x170, 'video_url': t.video.url if t.video else ''})
        return JSONResponse(str(form.errors))
    if request.is_ajax():
        return render(request, 'course_create/course_create_objects/course_create_dashboard_%s.html' %page, {'page': page, 'course': course, 'topic_list': str(json.dumps([k for k in Topic.objects.order_by('title').values('id', 'title').all()]))})
    else:
        return render(request, 'course_create/course_create_dashboard.html', {'page': page, 'course': course, 'topic_list': str(json.dumps([k for k in Topic.objects.order_by('title').values('id', 'title').all()]))})


@authenticated_user
def course_create_part(request, page, uuid=None):
    if page =='addpart':
        course = get_object_or_404(Course, uuid=uuid)
        coursepart = CoursePart.objects.create(course=course, title='Untitled Part')
        msg = 'Course Part Created'
    elif page == 'updatepart':
        coursepart = get_object_or_404(CoursePart, uuid=uuid)
        course = coursepart.course
        msg = 'Course Part Updated'
    if request.method=="POST":
        if not coursepart:
            coursepart = CoursePart(course=course)
        form = CourseFormDict[page](request.POST, instance=coursepart)
        if form.is_valid():
            t = form.save()
            part_res = {i.order: i.id for i in t.resources.all()}
            course.part_count = course.parts.count()
            course.paid_part_count = course.parts.filter(is_paid=True).count()
            if course.paid_part_count>0:
                course.is_paid=True
            course.save()
            return JSONResponse({'status': 'success', 'msg': msg, 'part_url': reverse('course_create_dashboard_add_part', args=('updatepart', t.uuid)), 'part_id': t.uuid, 'p_ids': part_res})
    if request.is_ajax():
        template = 'course_create/course_create_objects/course_create_dashboard_add_part.html'
    else:
        template = 'course_create/course_create_dashboard.html'
    return render(request, template, {'page': page, 'course': course, 'coursepart': coursepart})

@authenticated_user
def course_create_faq(request, page, uuid=None):
    if uuid:
        course = get_object_or_404(Course, uuid=uuid)
    else:
        course = None        

    if request.method=="POST":
        faq_id = request.POST.get('id', None)
        if faq_id:
            faq = get_object_or_404(CourseFaq, id=uuid)
            msg = 'Faq Updated'
        else:
            faq = CourseFaq(course=course)
            msg = 'Faq Created'
        form = CourseFormDict[page](request.POST, instance=faq)
        if form.is_valid():
            t = form.save()
            return JSONResponse({'status': 'success', 'msg': msg, 'faq_id': t.id, 'delete_url': reverse('faq_delete')})
        return HttpResponse(str(form.errors))
    if request.is_ajax():
        if course.is_offline:
            template = 'course_create/offline_course/objects/course_faqs.html'
        else:
            template = 'course_create/course_create_objects/course_create_dashboard_faqs.html'
    else:
        if course.is_offline:
            template = 'course_create/offline_course/course_create_dashboard.html'
        else:
            template = 'course_create/course_create_dashboard.html'
    return render(request, template, {'page': page, 'course': course})

@authenticated_user
@only_POST
def faq_delete(request):
    faq_id = request.REQUEST.get('id', None)
    if not faq_id:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    try:
        faq = CourseFaq.objects.get(id=faq_id)
        if faq.course.user == request.user:
            faq.delete()
            return JSONResponse({'status': 'success', 'msg': 'Faq Deleted'})
        else:
            return JSONResponse({'status': 'error', 'msg': 'You Sir do not have permission to delete this question.'})
    except:
        return JSONResponse({'status': 'error', 'msg': 'Faq Doesnt exists.'})  

@authenticated_user
@only_POST
def course_part_resource_delete(request):
    cpr_id = request.REQUEST.get('p_id', None)
    if not cpr_id:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    try:
        cpr = CoursePartResource.objects.get(id=cpr_id)
        if cpr.course.user == request.user:
            cpr.delete()
            return JSONResponse({'status': 'success', 'msg': 'Course Part content Deleted'})
        else:
            return JSONResponse({'status': 'error', 'msg': 'You Sir do not have permission to delete this Course Part content.'})
    except:
        return JSONResponse({'status': 'error', 'msg': 'Course Part content Doesnt exists.'})  


@authenticated_user
@only_POST
def course_lecture_delete(request):
    cpr_id = request.REQUEST.get('p_id', None)
    if not cpr_id:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    try:
        cpr = CourseLecture.objects.get(id=cpr_id)
        if cpr.course.user == request.user:
            cpr.delete()
            return JSONResponse({'status': 'success', 'msg': 'Course Lecture Deleted'})
        else:
            return JSONResponse({'status': 'error', 'msg': 'You Sir do not have permission to delete this Course Lecture.'})
    except:
        return JSONResponse({'status': 'error', 'msg': 'Course Lecture Doesnt exists.'})  


@authenticated_user
@only_POST
def course_part_delete(request):
    cp_id = request.REQUEST.get('id', None)
    if not cp_id:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    try:
        cp = CoursePart.objects.get(uuid=cp_id)
        if cp.course.user == request.user:
            course = cp.course
            # next url to be changed to next section
            next_url = reverse('course_create_dashboard', args=('overview', course.uuid))
            cp.delete()
            return JSONResponse({'status': 'success', 'msg': 'Course Part Deleted', 'next_url': next_url})
        else:
            return JSONResponse({'status': 'error', 'msg': 'You Sir do not have permission to delete this Course Part content.'})
    except:
        return JSONResponse({'status': 'error', 'msg': 'Course Part Doesnt exists.'})  

@authenticated_user
@only_POST
def course_delete(request):
    cp_id = request.REQUEST.get('id', None)
    if not cp_id:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    try:
        cp = Course.objects.get(uuid=cp_id)
        if cp.user == request.user:
            cp.delete()
            return JSONResponse({'status': 'success', 'msg': 'Course Deleted'})
        else:
            return JSONResponse({'status': 'error', 'msg': 'You Sir do not have permission to delete this Course.'})
    except:
        return JSONResponse({'status': 'error', 'msg': 'Course Doesnt exists.'})  

@authenticated_user
@only_POST
def update_course_part_order(request, uuid):
    course = get_object_or_404(Course, uuid=uuid)
    cp_order = request.REQUEST.get('list', None)
    if not cp_order:
        return JSONResponse({'status': 'error', 'msg': 'value cant be none'})
    if course.user != request.user:
        return JSONResponse({'status': 'error', 'msg': 'You Sir do not have permission to delete this Course.'})
    cp_order_dict = json.loads(cp_order)
    for id, order in cp_order_dict.items():
        t = CoursePart.objects.filter(uuid=id).update(order=order)
    return JSONResponse({'status': 'success', 'msg': 'Order updated'})

@authenticated_user
def course_coursepart_dashboard(request, uuid):
    coursepart = get_object_or_404(CoursePart, uuid=uuid)
    course = coursepart.course

    if request.method == "POST" and course:
        form = CourseFormDict[page](request.POST, instance=course)
        if form.is_valid():
            t = form.save()
            return JSONResponse({'status': 'success', 'msg': 'Course Part created'})

    if request.is_ajax():
        return render(request, 'course_create/course_create_objects/course_create_dashboard_%s.html' % page, {'page': page, 'course': course, 'coursepart': coursepart})
    else:
        return render(request, 'course_create/course_create_dashboard.html', {'page': page, 'course': course, 'coursepart': coursepart})


@authenticated_user
def course_manage_dashboard(request, page=None, uuid=None):
    if uuid:
        course = get_object_or_404(Course, uuid=uuid)
    else:
        course = None
    if request.method == "POST" and course:
        form = CourseFormDict[page](request.POST, instance=course)
        if form.is_valid():
            t = form.save()
            return JSONResponse({'status': 'success', 'msg': '%s updated' % page})
        else:
            return HttpResponse('<form action="." method="GET">'+str(form)+'<input type="submit" value="submit"\></form>')
    return render(request, 'course_manage/course_manage_dashboard.html', {'page': page, 'course': course})

def explore_courses(request, topic_slug='trending', template=None):

    paid = request.REQUEST.get('is_paid', None)
    is_offline = request.REQUEST.get('is_offline', None)
    is_expert = request.REQUEST.get('is_expert', None)
    city_value = request.REQUEST.get('city', 'All')
    if not topic_slug:
        raise Http404

    topic = None
    #cp = Course.objects.filter(is_published=True, is_deleted=False)
    cities_list = ['All'] + list(Course.objects.filter(city__isnull=False).values_list('city', flat=True).distinct())
    cp = Course.objects.filter(is_deleted=False).filter(is_published=True).filter(Q(is_offline=False)|Q(is_offline=True, start_datetime__gte=datetime.now())|Q(is_offline=True, start_datetime__isnull=True))
    if topic_slug == 'you':
        if request.user and request.user.is_authenticated():
            featured_course = cp.exclude(id__in=Library.objects.filter(
                user=request.user, 
                content_type=ContentType.objects.get_for_model(Course)).values_list(
                    'object_id', flat=True)).filter(
                        topic__in=request.user.followed_topics.all()).distinct().order_by('-date_modified')
            #featured_course = list(featured_course)
            #if len(featured_course) < 8:
            if featured_course.count() < 8:
                featured_course = cp.filter(topic__in=request.user.followed_topics.all()).distinct().order_by('-date_modified')
        else:
            featured_course = cp.order_by('-date_modified')
    elif topic_slug == 'trending':  # trending
        featured_course = cp.order_by('-view_count')
    elif topic_slug == 'recent':  # recent
        featured_course = cp.order_by('-date_created')
    elif topic_slug == 'top':  # top
        featured_course = cp.order_by('-view_count')
    else:
        topic, redirect = get_by_slug_or_404(Topic, topic_slug)
        if redirect:
            return HttpResponsePermanentRedirect(reverse('explore_courses', args=(topic.uslug(),)))
        for c in topic.course_set.all():
            print 'Pre: ', c.title, c.is_published
        featured_course = topic.course_set.all().filter(is_published=True, is_private=False).order_by('-date_modified')
        for c in featured_course:
            print 'Post: ', c.title, c.is_published

    if topic and is_expert=='1':
        expert_list = User.objects.filter(followed_topics=topic)
    else:
        expert_list = []

    if paid == '1':
        featured_course = featured_course.filter(is_paid=1)
    elif paid == '0':
        featured_course = featured_course.filter(is_paid=0)

    """
    7/11/13: Added to get search Explore course working
    """
    searchTerm = request.GET.get('search_term', '')
    if searchTerm:
        wildcard = searchTerm
        featured_course = featured_course.filter(title__icontains=wildcard, is_published=True, is_private=False)
    """
    end 7/11/2013
    """

    if is_offline == '1':
        featured_course = featured_course.filter(is_offline=1)
        if city_value != 'All':
            featured_course = featured_course.filter(city=city_value)
    elif is_offline == '0':
        featured_course = featured_course.filter(is_offline=0)

    if is_expert=='1':
        p, p1, window, page, perpage = paginate_from_request(plist=expert_list, request=request, default_perpage=20)  # @UnusedVariable
    else:
        p, p1, window, page, perpage = paginate_from_request(plist=featured_course, request=request, default_perpage=20)  # @UnusedVariable

    f = {
         'topics': Topic.objects.annotate(num=Count('course')).exclude(num=0).order_by('title').all(),
         'topic_list': str(json.dumps([k for k in Topic.objects.extra({'topicslug': 'if(slugno>1, concat(slug, "~", slugno), slug)'}).order_by('title').values('id', 'title', 'topicslug').all()])),
         'featured_course': p1.object_list, 
         'results': p1, 
         'num_pages': p.num_pages, 
         'object_count': p.count, 
         'page': page,  
         'learn_tab': True,
         'topic': topic, 
         'topic_name': topic.titlecase() if topic else topic_slug.title(), 
         'topic_id': None if not topic else topic.id, 
         'topic_slug': topic_slug.title() if topic else None, 
         'window': window, 
         'courses': p1.object_list,
         'user_list': p1.object_list, 
         'explore_course_page': True,
         'cities_list': cities_list,
         'city_value': city_value,
         }

    if not template:
        if request.is_ajax():
            template = 'explore_courses/explore_courses_ajax.html'
            if is_expert=='1':
                template = 'explore_users/explore_user_ajax.html'
            return TemplateResponse(request, template, f)

        else:
            template = 'explore_courses/explore_courses.html'
            return render(request, template, f)


#    courses = Course.objects.filter(is_published=True, is_deleted=False)
    #return TemplateResponse(request, template, f)

def course_sections(request):
    course_id = request.REQUEST.get('id', None)
    crs = get_object_or_404(Course, pk=course_id)
    from apps.payment.utils import can_view
    if crs.user == request.user or can_view(request.user, crs):
        Courseparts = CoursePart.objects.filter(course_id=course_id).order_by('order')
    else:
        Courseparts = CoursePart.objects.filter(course_id=course_id, is_paid=False).order_by('order')
    course_parts = [CoursePartsSerializer(c).get_data(request.user) for c in Courseparts]
    res={}
    res['sections'] = course_parts
    return JSONResponse(res)

def course_part_posts(request):
    cp_id = request.REQUEST.get('id', None)

    coursepart = get_object_or_404(CoursePart, id=cp_id)
    res = {"notes": []}
    if coursepart.keys_list:
        keys_list = [ key for key in coursepart.keys_list.split(',') if key ]
    else:
        keys_list = []
        
    for key in keys_list:
        k = {'key': key, 'note_id': '', 'note': ''}
        if request.user.is_authenticated():
            for note in Note.objects.filter(content_type=ContentType.objects.get_for_model(coursepart), object_id=coursepart.id, note_key=key, user=request.user):
                k['note_id'], k['note'] = note.id, note.note
                res["notes"].append(deepcopy(k)) 
        if not k['note_id']:
            res["notes"].append(k)

    # res["notes"] = [
    #     {
    #         "key": "8ksseudidc",
    #         "note": "Ahhhh....I am so filled with soy. get it out.",
    #     },
    #     {
    #         "key": "6486klza6k",
    #         "note": "Yo mama. lets do some karamba and tunga, with bongo drums" 
    #     },
    #     {
    #         "key": "11f8rpjvrh",
    #         "note": "Pig me",
    #     },
    #     {
    #         "key": "7utjjxcevk",
    #         "note": "Human me" 
    #     } 
    # ]
    return JSONResponse(res)
    
@authenticated_user
@only_POST
def course_part_rate(request, uuid):
    rating = get_int_or_default_from_request(request, 'rating', 1)
    if not rating:
        return JSONResponse({'status': 'error', 'msg': 'Invalid response'})

    course_part = get_object_or_404(CoursePart, uuid=uuid)

    try:
        course_part_follower = CoursePartFollower.objects.get(user=request.user, course_part=course_part) 
    except:
        return JSONResponse({'status': 'error', 'msg': 'Please read the part First.'})

    course_part_rate= CoursePartRate.objects.get_or_create(course_part=course_part, user=request.user)[0] 
    course_part_rate.rating = rating
    course_part_rate.save()

    CoursePart.objects.filter(id=course_part.id).update(rating=course_part.ratings.aggregate(Avg('rating'))['rating__avg'])
    Course.objects.filter(id=course_part.course_id).update(rating=CoursePart.objects.filter(course=course_part.course, rating__gt=0).aggregate(Avg('rating'))['rating__avg'])
    course_review = CourseReview.objects.get_or_create(course=course_part.course, user=request.user)[0]
    CourseReview.objects.filter(id=course_review.id).update(rating=CoursePartRate.objects.filter(course_part__course=course_part.course, user=request.user).aggregate(Avg('rating'))['rating__avg'])
    rating_avg = get_object_or_404(CoursePart, uuid=uuid).rating
    rating_count =  CoursePartRate.objects.filter(course_part=course_part).count()
    return JSONResponse({'status': 'success', 'msg': 'Done', 'rating_avg':rating_avg, 'rating_count': rating_count})

@authenticated_user
@only_POST
def course_review_post(request, uuid):
    course = get_object_or_404(Course, uuid=uuid)
    review = request.REQUEST.get('review', '')

    course_review = CourseReview.objects.get_or_create(course=course, user=request.user)[0]
    course_review.review = review
    course_review.save()

    return JSONResponse({'status': 'success', 'msg': 'Review Posted.'})


def course_author_search(request):
    query = request.REQUEST.get('q', None)
    if not query:
        users = User.objects.filter(is_staff=True)
    else:
        users = User.objects.filter(Q(first_name__icontains=query)|Q(last_name__icontains=query), is_staff=True)

    return JSONResponse([{
                            'id': i.id,
                            'full_name': i.get_full_name(),
                            'slug': i.fbuser.uslug(),
                            'bio': i.fbuser.get_bio(),
                            'image': i.fbuser.userpic_square(),
                            'profile_url': i.fbuser.url(),
                            'module_name': i._meta.module_name,
                            'app_label': i._meta.app_label,
        } for i in users])



@only_GET
@authenticated_user(type='redirect', text_content='Please connect via Facebook to view user profile')
def my_courses(request):
    page_type = request.REQUEST.get('filter', None)
    if page_type == None:
        page_type = 'pub'

    if page_type == 'learning':
        courses = Course.objects.filter(followers__user=request.user).exclude(is_deleted=True).exclude(user=request.user).order_by('-date_modified')
    
    elif page_type == 'pub':
        courses = Course.objects.filter(user=request.user, is_published=True).exclude(is_deleted=True).order_by('-date_modified')

    else:
        courses = Course.objects.filter(user=request.user, is_published=False).exclude(is_deleted=True).order_by('-date_modified')        
    
    """if page_type != 'learning':
        courses = Course.objects.filter(user=request.user).exclude(is_deleted=True).order_by('-date_modified')
        #lessons = Course.objects.filter(user=request.user).exclude(deleted=True).order_by('-date_modified')
        #if page_type in ['dra', 'pen', 'pub']:
        if page_type == 'pub':
            courses = courses.filter(is_published=True)
        else:
            courses = courses.filter(is_published=False)
    else:
        courses = Course.objects.filter(followers__user=request.user).exclude(is_deleted=True).exclude(user=request.user).order_by('-date_modified')"""
    
    p, p1, window, page, pp = paginate_from_request(plist=courses, request=request, default_perpage=16)  # @UnusedVariable
    try:
        profile = request.user.fbbasicprofile
    except:
        profile = None
    if request.is_ajax():
        template = 'course/userprofile/my_courses_ajax.html'
    else:
        template = 'course/userprofile/my_courses.html'
    return render(request, template, {
                                    'results': p1,
                                    'courses': courses,
                                    'profile': profile,
                                    'num_pages': p.num_pages,
                                    'filter': page_type,
                                    })



def plan(request):
    return render(request, 'plan/plan_home.html', {'debug': settings.DEBUG})


from models import *



# Offline courses views
def course_create_offline(request, page=None, uuid=None):
    if uuid:
        course = get_object_or_404(Course, uuid=uuid)
    else:
        course = None
    if request.method=="POST" and course:
        form = CourseFormDict[page](request.POST, request.FILES, instance=course)
        if form.is_valid():
            t = form.save()
            return JSONResponse({'status': 'success', 'msg': '%s updated' %page, 'thumbnail_url': t.thumb_or_default__230x170, 'video_url': t.video.url if t.video else ''})
        return JSONResponse(str(form.errors))
    if request.is_ajax():
        template = 'course_create/offline_course/objects/course_%s.html' %page
        #return render(request, 'course_create/offline_course/objects/course_%s.html' %page, {'page': page, 'course': course, 'countries': COUNTRIES})
    else:
        template = 'course_create/offline_course/course_create_dashboard.html'
    return render(request, template, {
        'page': page, 
        'course': course, 
        'topic_list': json.dumps([{
            'id': k.id, 
            'title': k.title, 
            'slug': reverse('explore_courses', args=(k.uslug(),))
            } for k in Topic.objects.order_by('title').all()]),
        'addedtopics': json.dumps([{
            'id': k.id, 
            'title': k.title, 
            'slug': reverse('explore_courses', args=(k.uslug(),))
            } for k in course.ordered_topics()]),
        'countries': COUNTRIES,
        })

def course_create_offline_lectures(request, page=None, uuid=None):
    if uuid:
        course = get_object_or_404(Course, uuid=uuid)
    else:
        course = None
    if request.method=="POST" and course:
        lectures = request.REQUEST.get('lectures', None)
        if not lectures:
            return JSONResponse({'status': 'error', 'msg': 'lectures cannot be null'})
        lectures = json.loads(lectures)
        for i in lectures:
            if 'id' in i and i['id']:
                course_lecture = CourseLecture.objects.get(id=i['id'])
            else:
                course_lecture = CourseLecture(course=course)
            form = CourseFormDict[page](i, instance=course_lecture)
            if form.is_valid():
                t = form.save()
        lecture_ids = {i.order: i.id for i in course.lectures.all()}
        return JSONResponse({'status': 'success', 'msg': '%s updated' %page, 'lecture_ids': lecture_ids})
    if request.is_ajax():
        template = 'course_create/offline_course/objects/course_%s.html' %page
    else:
        template = 'course_create/offline_course/course_create_dashboard.html'
    return render(request, template, {
        'page': page, 
        'course': course, 
        'topic_list': json.dumps([{
            'id': k.id, 
            'title': k.title, 
            'slug': reverse('explore_courses', args=(k.uslug(),))
            } for k in Topic.objects.order_by('title').all()]),
        'addedtopics': json.dumps([{
            'id': k.id, 
            'title': k.title, 
            'slug': reverse('explore_courses', args=(k.uslug(),))
            } for k in course.ordered_topics()]),
        'countries': COUNTRIES,
        })


@only_POST
def course_edit_offline_lecture_title(request):
    if request.method=="POST" and course:
        lecture_id = request.REQUEST.get('id', None)
        lecture = get_object_or_404(CourseLecture, id=lecture_id)
        form = CourseEditLectureTitleForm(request.POST, instance=lecture)
        if form.is_valid():
            t = form.save()
        return JSONResponse({'status': 'success', 'msg': 'lecture title updated'})


def course_page_offline(request):
    return render(request, 'course/offline_course/course_dashboard.html', {})


@authenticated_user
@only_POST
def mark_as_done(request):
    app_label = request.REQUEST.get('app_label', None)
    module_name = request.REQUEST.get('module_name', None)
    object_id = request.REQUEST.get('object_id', None)
    action = request.REQUEST.get('action', None)


    if not app_label or not module_name or not object_id or not action:
        return JSONResponse({'status': 'error', 'msg': 'invalid parameters'})

    try:
        content_type = ContentType.objects.get(app_label=app_label, model=module_name)
        obj = content_type.get_object_for_this_type(id=object_id)
    except:
        return JSONResponse({'status': 'error', 'msg': 'object doesnt exists.'}, status=404)

    if action =='mark':
        if module_name == 'coursepart':
            t = CoursePartDone.objects.get_or_create(course_part=obj, user=request.user)
        elif module_name == 'coursepartresource':
            t = CoursePartDone.objects.get_or_create(course_part_resource=obj, user=request.user)
        else:
            return JSONResponse({'status': 'error', 'msg': 'invalid models'})
    elif action=="unmark":
        if module_name == 'coursepart':
            t = CoursePartDone.objects.get(course_part=obj, user=request.user)
        elif module_name == 'coursepartresource':
            t = CoursePartDone.objects.get(course_part_resource=obj, user=request.user)
        else:
            return JSONResponse({'status': 'error', 'msg': 'invalid models'})
        t.delete()
        return JSONResponse({'status': 'success', 'msg': 'Unmarked'})
    else:
        JSONResponse({'status': 'error', 'msg': 'Invalid options'})        

    return JSONResponse({'status': 'success', 'msg': 'success'})

@only_POST
def request_callback_for_offline_course(request):
    name = request.REQUEST.get('name', None)
    email = request.REQUEST.get('email', None)
    contact_number = request.REQUEST.get('contact_number', None)
    course_id = request.REQUEST.get('course_id', None)
    description = request.REQUEST.get('description', None)

    if not name or not email or not contact_number or not course_id:
        return JSONResponse({'status': 'error', 'msg': 'invalid parameters'})

    try:
        course = Course.objects.get(id=course_id)
    except:
        return JSONResponse({'status': 'error', 'msg': 'course doesnt exist.'})

    callback = CourseEnquiry.objects.create(name=name, email=email, contact_number=contact_number, course=course, description=description)

    return JSONResponse({'status': 'success', 'msg': 'Callback Request Registered'})


@authenticated_user
@only_POST
def ask_question(request):
    form = QuestionForm(request.POST, instance=Question(user=request.user))
    if form.is_valid():
        question = form.save()
        return JSONResponse({'status': 'success', 'msg': 'Question Created', 'question_id': question.id })
        #return HttpResponseRedirect(reverse('course_create_dashboard', args=('getstarted', t.uuid)))
    return JSONResponse({'status': 'error', 'msg': 'Question and Course Section is not valid', 'question_id': ''})

@authenticated_user
@only_POST
def answer_question(request):
    form = AnswerForm(request.POST, instance=Answer(user=request.user))
    if form.is_valid():
        answer = form.save()
        return JSONResponse({'status': 'success', 'msg': 'Answer Created', 'answer_id': answer.id })
        #return HttpResponseRedirect(reverse('course_create_dashboard', args=('getstarted', t.uuid)))
    return JSONResponse({'status': 'error', 'msg': 'Answer or Question is not valid', 'answer_id': ''})

def section_questions(request):
    section_id = request.REQUEST.get('id', None)
    if not section_id:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    course_part = CoursePart.objects.get(id=section_id)
    return render(request, 'course/section_page_discussion.html', {'course_part': course_part})

def question_answers(request):
    qid = request.REQUEST.get('id', None)
    if not qid:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    question = Question.objects.get(id=qid)
    return render(request, 'course/question_answer_ajax.html', {'question': question})

@authenticated_user
@only_POST
def section_question_delete(request):
    qid = request.REQUEST.get('id', None)
    if not qid:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    question = Question.objects.get(id=qid)
    if question.user == request.user:
        question.delete()
        return JSONResponse({'status': 'success', 'msg': 'Question Deleted.'})
    return JSONResponse({'status': 'error', 'msg': 'Sir you do not have permission to delete this question.'})

@authenticated_user
@only_POST
def section_answer_delete(request):
    qid = request.REQUEST.get('id', None)
    if not qid:
        return JSONResponse({'status': 'error', 'msg': 'No id provided'})
    answer = Answer.objects.get(id=qid)
    if answer.user == request.user:
        answer.delete()
        return JSONResponse({'status': 'success', 'msg': 'Answer Deleted.'})
    return JSONResponse({'status': 'error', 'msg': 'Sir you do not have permission to delete this answer.'})

##############################################################################################
##                                                                                          ##
##                                         Not In use                                       ##
##                                                                                          ##
##############################################################################################


def course_create_guidelines(request):
    return render(request, 'course_create/course_create_step2.html', {'page': 'guidelines'})


def course_create_thumbnail(request):
    return render(request, 'course_create/course_create_step2.html', {'page': 'thumbnail'})

def course_create_faqs(request):
    return render(request, 'course_create/course_create_step2.html', {'page': 'faqs'})

def course_with_subscription(request):
    return render(request, 'course/course_page_with_subscription.html', {})

def course_consume(request):
    return render(request, 'course/course_consume.html', {})




@authenticated_user
def course_announce_view(request, course_id):
    course = get_object_or_404(Course, pk=course_id)
    if request.method == 'POST':
        txt = request.POST.get('announcement', '')
        if not txt:
            return HttpResponse(status=400)

        c = MyAnnouncement(user=request.user, text=txt, course=course)
        try:
            c.save()
        except Exception:
            return render(request, 'course_manage/course_announce.html', {
                'course': course,
                'error': 'Unable to save to now. Please try later.',
                'announcements': MyAnnouncement.objects.filter(course=course),       
            })

    return render(request, 'course_manage/course_announce.html', {
        'course': course,
        'announcements': MyAnnouncement.objects.filter(course=course),
    })


@authenticated_user
@only_GET
def isPartDoneByUser(request):
    sectid = request.GET.get('id', 0)
    part = get_object_or_404(CoursePart, pk=sectid)
    try:
        obj = CoursePartDone.objects.get(user=request.user, course_part=part)
        res = True
    except CoursePartDone.DoesNotExist:
        res = False

    return JSONResponse({'status': res})